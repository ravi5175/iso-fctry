#!/bin/bash

[[ ! -e .version ]] && echo "1.1" > .version
[[ ! -e .buildno ]] && echo "1" > .buildno

VERSION=$(cat .version)
BUILD_NO=$(cat .buildno)

[[ -z "${1}" ]] && {
	echo "${VERSION}-${BUILD_NO}"
	exit 0
}

case "${1}" in
	+v)
		VERSION=$(echo "$VERSION + 0.1" | bc)
		echo $VERSION > .version
		;;
	+b)
		((BUILD_NO=BUILD_NO+1))
		echo $BUILD_NO > .buildno
		;;
esac

echo "${VERSION}-${BUILD_NO}"
	
		
