#/bin/bash

_devenv=${DEVENV:-"$(pwd)/devel"}

_src="${1}"

if [[ -z "${2}" ]] ; then
    if [[ -d "${_src}" ]] ; then
        _dest="$(dirname ${1})"
        mkdir -p ${_dest}
    else
        _dest="${1}"
    fi
fi

cp -av ${_src} ${_devenv}/${_dest}