#!/bin/bash

ROOTFS="$(pwd)/devel"
[[ -z "${1}" ]] || ROOTFS="${1}"

[[ -d "${ROOTFS}" ]] || {
    echo "dev-environment not found ${ROOTFS}"
    exit 1
}


_xchroot() {
    local _rdir="${1}"
    shift
    local _args="${@}"

    mkdir -p ${_rdir}/{dev,proc,sys,run}
    mount --bind /dev ${_rdir}/dev
    mount --bind /sys ${_rdir}/sys
    mount -t proc proc ${_rdir}/proc

    xhost +local:
    chroot ${_rdir} /usr/bin/env -i \
        HOME=/root                  \
        TERM="$TERM"                \
        DISPLAY="$DISPLAY"          \
        PATH=/bin:/usr/bin:/sbin:/usr/sbin:/apps/bin/.env   \
        PS1="[rlxos-$(basename ${ROOTFS}) \W] \$ " \
    $_args

    sleep 1
    umount ${_rdir}/{sys,proc,dev}
}


_xchroot ${ROOTFS} /bin/bash