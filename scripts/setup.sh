#!/bin/bash
[[ "$UID" != 0 ]] && {
    echo "need root permission"
    exit 1
}

check_header() {
    local header_file="${1}"
    [[ -z "$(find /usr/include -type f -name "${header_file}")" ]]
}

check_library() {
    local library="${1}"
    [[ -z "$(find /usr/lib /usr/lib64 /usr/local/lib -type f -name "${library}*")" ]]
}

check_binary() {
    local bin="${1}"
    which $bin && return 1 || return 0
}

check_req() {

    echo "** checking required headers"
    for i in md5 curl ; do
        if check_header "${i}.h" ; then
            echo "Error: ${i}.h header file is missing"
            exit 1
        fi
    done

    echo "** checking required libraries"
    for i in curl ssl crypto ; do
        if check_library "lib${i}.so" ; then
            echo "Error: lib${i}.so is missing"
            exit 1
        fi
    done

    echo "** checking required binaries"
    for i in gcc g++ pkg-config wget; do
        if check_binary "${i}" ; then
            echo "Error: $i is missing"
            exit 1
        fi
    done
}

install_librlx() {
    local tmpdir=$(mktemp -d /tmp/librlx.XXXXXX)
    local _srcdir="${tmpdir}/src"

    echo "=> Download source file"
    wget "https://gitlab.com/rlxos/softwares/librlx/-/archive/master/librlx-master.tar.gz" -P ${_srcdir}
    tar -xaf ${_srcdir}/librlx-master.tar.gz -C ${tmpdir}

    cd ${tmpdir}/librlx-master
    make install PREFIX=/usr
    rtn=$?
    rm -rf ${tmpdir}

    return $rtn
}

install_project() {
    local tmpdir=$(mktemp -d /tmp/project.XXXXXX)
    local _srcdir="${tmpdir}/src"

    echo "=> Download source file"
    wget "https://gitlab.com/rlxos/softwares/project/-/archive/master/project-master.tar.gz" -P ${_srcdir}
    tar -xaf ${_srcdir}/project-master.tar.gz -C ${tmpdir}

    cd ${tmpdir}/project-master
    g++ src/*.cc -I include -o /usr/bin/project /usr/lib/librlx.a -lssl -lcrypto -lstdc++fs -std=c++17
    rtn=$?
    rm -rf ${tmpdir}

    return $rtn
}

install_appctl() {
    local tmpdir=$(mktemp -d /tmp/appctl.XXXXXX)
    local _srcdir="${tmpdir}/src"

    echo "=> Download source file"
    wget "https://gitlab.com/rlxos/softwares/appctl/-/archive/master/appctl-master.tar.gz" -P ${_srcdir}
    tar -xaf ${_srcdir}/appctl-master.tar.gz -C ${tmpdir}

    cd ${tmpdir}/appctl-master
    project do build
    install -vDm755 build/appctl -t /usr/bin/
    install -vDm644 build/libapp.so -t /usr/lib/
    install -vDm755 src/rcp/rcp-compiler.sh /usr/bin/rcp-compiler
    install -vDm755 build/rlxpkg -t /usr/lib/appctl/
    install -vDm755 build/rcp -t /usr/lib/appctl/
    rtn=$?
    rm -rf ${tmpdir}

    return $rtn
}

check_req

if check_library "librlx" ; then
    echo "** Compiling librlx"
    install_librlx
    if [[ $? != 0 ]] ; then
        echo "Error: failed to install librlx"
        exit 1
    fi
fi

if check_binary "project" ; then
    echo "** compiling project"
    install_project
    if [[ $? != 0 ]] ; then
        echo "Error: failed to install project"
        exit 1
    fi
fi

if check_binary "appctl" ; then
    echo "** compiling appctl"
    install_appctl
    if [[ $? != 0 ]] ; then
        echo "Error: failed to install appctl"
        exit 1
    fi
fi

ldconfig -v
