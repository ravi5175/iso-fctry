#!/bin/bash
[[ "$UID" != 0 ]] && {
    echo "need root permission"
    exit 1
}

[[ -z "${1}" ]] && {
    echo "Usage: rootfs.sh <type>"
    exit 1
}

[[ ! -f "config/app.list" ]] && {
    echo "No config/app.list found"
    exit 1
}

trap "interrupt" 1 2 3 5 15

interrupt() {
	echo "user cancel"
	clean
	exit 5
}

VERSION=$(./scripts/version.sh +b)
TYPE="${1}"
ROOTS="$(mktemp -d tmp/${TYPE}.XXXXXXXXXX)"
UNICODE_FONTS="/usr/share/grub/unicode.pf2"
MODULES="linux normal iso9660 biosdisk memdisk search"
INSTALL_MODULES="$MODULES tar ls"

[[ -d build ]] || mkdir -pv build
clean() {
    echo "-> cleaning rootfs"
    rm -rf ${ROOTS}
}

sed "s|@root@|$ROOTS|" config/appctl.template.conf > config/appctl.${TYPE}.conf

_appctl() {
    appctl $@ config="$(pwd)/config/appctl.${TYPE}.conf"
    if [[ $? != 0 ]] ; then
        echo "Error: failed to do '$@'"
        clean
        exit 1
    fi
}

_appctl sync

appslist=$(cat config/app.list)
[[ -f config/app.${TYPE}.list ]] && {
	appslist="${appslist} $(cat config/app.${TYPE}.list)"
}

for i in $appslist ; do
    if [[ ! -d "$ROOTS/var/lib/app/index/$i" ]] ; then
        _appctl install $i    
    fi 
done

case $TYPE in
	docker|rootfs)
		COMPRESS="tar"
		;;
	iso*)
		COMPRESS="squa"
		;;
	devel*)
		COMPRESS="NON"
		;;
esac

COMPRESS=${COMPRESS:-"tar"}

THISDIR="${PWD}"

if [[ -f scripts/fixes.sh ]] ; then
	echo "executing common fixes"
	(cd ${ROOTS}; set -e -x; sh ${THISDIR}/scripts/fixes.sh)
	if [[ $? != 0 ]] ; then
		echo "Error failed to execute fixes"
		clean
		exit 1
	fi
fi

if [[ -f scripts/fixes."${TYPE}".sh ]] ; then
	echo "executing common fixes"
	(cd ${ROOTS}; set -e -x; sh ${THISDIR}/scripts/fixes.${TYPE}.sh)
	if [[ $? != 0 ]] ; then
		echo "Error failed to execute fixes"
		clean
		exit 1
	fi
fi

echo " echo -e 'rlxos\nrlxos' | passwd" | chroot ${ROOTS}
[[ $? != 0 ]] && {
	echo "Failed to set root user password"
	umount "${ROOTS}"/{proc,sys,dev} -l
	_clean
	exit 2
}

echo "groupadd -g 21 gdm && useradd -c 'GDM Daemon Owner' -d /var/lib/gdm -u 21 -g gdm -s /bin/false gdm

groupadd -g 71 colord
useradd -c 'Color Daemon Owner' -d /var/lib/colord -u 71 -g colord -s /bin/false colord

groupadd -fg 27 polkitd
useradd -c 'PolicyKit Daemon Owner' -d /etc/polkit-1 -u 27 -g polkitd -s /bin/false polkitd

glib-compile-schemas /usr/share/glib-2.0/schemas
gdk-pixbuf-query-loaders --update-cache
gtk-update-icon-cache /usr/share/icons/Adwaita/ -f
gtk-update-icon-cache /usr/share/icons/hicolor/ -f
update-mime-database /usr/share/mime

localedef -i en_IN -f UTF-8 en_IN.UTF-8
"  | chroot ${ROOTS}
[[ $? != 0 ]] && {
	echo "Failed to add users"
	umount "${ROOTS}"/{proc,sys,dev} -l
	_clean
	exit 2
}


if [[ $COMPRESS == "tar" ]] ; then
    	(cd ${ROOTS}; tar -caf build/rlx-${TYPE}-${VERSION}-$(uname -m).tar.gz *)
		rtn=$?
		if [[ $rtn != 0 ]] ; then
			echo "Error Failed to compress system"
		fi
		clean
		exit $rtn
elif [[ ${COMPRESS} == "NON" ]] ; then
	[[ -d "$(pwd)/${TYPE}" ]] && {
		echo "Development environment is already setup at $(pwd)/${TYPE}"
		echo "Press 'y' to delete it"
		read resp
		if [[ ${resp} == 'y' ]] ; then
			echo "cleaning old cache"
			rm -r ${TYPE}
		else
			echo "process exit"
			clean
			exit $rtn
		fi
	}
	mv ${ROOTS} $(pwd)/${TYPE}
	clean
	echo "Process completed"
	exit $rtn
fi


echo ":: Generating squa"
SQUA_IMAGE="$(pwd)/build/rlx-${TYPE}-${VERSION}-$(uname -m).rootfs"
OVERLAY_IMAGE="$(pwd)/build/iso.img"
ls 
[[ -e "${SQUA_IMAGE}" ]] && rm "${SQUA_IMAGE}"
mksquashfs ${ROOTS}/* "${SQUA_IMAGE}"

[[ -e "${OVERLAY_IMAGE}" ]] && rm "${OVERLAY_IMAGE}"
chown root:root overlay -Rv
mksquashfs overlay/* "${OVERLAY_IMAGE}"
chown "${USER}" overlay -Rv

isodir=$(mktemp -d tmp/iso.XXXXXXXXXX)
cache="$(mktemp -d tmp/cache.XXXXXXXXXX)"

    
_clean() {
	clean
	rm -rf "${isodir}"
	rm -rf "${cache}"
}

mkdir -p "${isodir}"/boot/grub
cp files/grub.cfg "${isodir}"/boot/grub
cp "${SQUA_IMAGE}" "${isodir}"/rootfs.img
cp "${OVERLAY_IMAGE}" "${isodir}"/iso.img

mkdir -p "${ROOTS}"/{dev,sys,proc,run,tmp}

mount --bind /dev "${ROOTS}"/dev
mount --bind /sys "${ROOTS}"/sys
mount -t proc proc "${ROOTS}"/proc

KERNEL_VERSION="$(ls ${ROOTS}/lib/modules/)"

[[ -z "${KERNEL_VERSION}" ]] && {
	echo "No kernel version found"
	umount "${ROOTS}"/{proc,sys,dev} -l
	_clean
	exit 2
}

ls -al ${ROOTS}

echo "mkinitrd -k=${KERNEL_VERSION} -iso" | chroot ${ROOTS}
[[ $? != 0 ]] && {
	echo "Failed to generate initrd"
	umount "${ROOTS}"/{proc,sys,dev} -l
	_clean
	exit 2
}


cp "${ROOTS}"/boot/vmlinuz-rlx "${isodir}"/boot/vmlinuz
cp "${ROOTS}"/boot/initrd "${isodir}"/boot/initrd

umount "${ROOTS}"/{proc,sys,dev} -l
touch "${isodir}"/rlxos

grub2-mkstandalone                           \
    --format=i386-pc                        \
    --output="${isodir}"/core.img            \
    --install-modules="$INSTALL_MODULES"    \
    --modules="$MODULES"                    \
    --locales=""                            \
    --fonts=""                              \
    "boot/grub/grub.cfg=files/grub.cfg"
[[ $? != 0 ]] && {
	echo "failed to build i386 boot image"
	_clean
	exit 1
}

cat /usr/lib/grub/i386-pc/cdboot.img "${isodir}"/core.img > "${isodir}"/bios.img

grub2-mkstandalone                           \
    --format=x86_64-efi                     \
    --output="${isodir}"/bootx64.efi         \
    --locales=""                             \
    --fonts=""                              \
    "boot/grub/grub.cfg=files/grub.cfg"
[[ $? != 0 ]] && {
	echo "failed to build x86_64 boot efi"
	_clean
	exit 1
}

(cd "${isodir}" && dd if=/dev/zero of=efiboot.img bs=1M count=10 && mkfs.vfat efiboot.img && mmd -i efiboot.img efi efi/boot && mcopy -i efiboot.img ./bootx64.efi ::efi/boot/ )

ls ${isodir}/ -al

xorriso                         \
    -as mkisofs                 \
    -iso-level  3               \
    -full-iso9660-filenames     \
    -volid "RLXOS"              \
    -eltorito-boot              \
    boot/grub/bios.img          \
    -no-emul-boot               \
    -boot-load-size 4           \
    -boot-info-table            \
    --eltorito-catalog          \
    boot/grub/boot.cat          \
    --grub2-boot-info           \
    --grub2-mbr                 \
    /usr/lib/grub/i386-pc/boot_hybrid.img \
    -eltorito-alt-boot          \
    -e EFI/efiboot.img          \
    -no-emul-boot               \
    -append_partition 2         \
    0xef                        \
    "${isodir}"/efiboot.img      \
    -output build/rlx-"${VERSION}"-${TYPE}-$(uname -m).iso \
    -graft-points \
	"${isodir}" \
    /boot/grub/bios.img="${isodir}"/bios.img \
    /EFI/efiboot.img="${isodir}"/efiboot.img


_clean
