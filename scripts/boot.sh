#!/bin/bash

[[ -e disk.qcow2 ]] || {
	echo "creating disk"
	qemu-img create -f qcow2 disk.qcow2 10G
}

[[ -z $DISK ]] && EXTRA="-boot d"
sudo qemu-system-x86_64	\
	-m 4G		\
	-vga virtio	\
	-display default,show-cursor=on	\
	-usb		\
	-device usb-tablet \
	-smp 2		\
	-cdrom $1	\
	-drive file=disk.qcow2,if=virtio \
	-cpu host \
	$EXTRA \
	-enable-kvm
