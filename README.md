# rlx-fctry
This tool generates bootable iso, rootfs, docker image, setup-development enviroment for rlxos using the app.<type>.list.

# Requirements
Below tools and libraries are need to setup development environment
- libcurl.so
- libssl.so

# Procedure
- execute setupenv.sh with root permissions. This script install [librlx](), [project]() and [appctl]() into the system
```
    sudo ./scripts/setup.sh
```

- prepare app.<type>.list for iso, rootfs, docker image as per requirements. A basic set of packages are listed in app.list. and for extra packages can be listed in app.<type>.list

## Compressed rootfs
To generate compressed rootfs tarball simply create a file app.<name>.list

## Bootable iso
For this <name> should start with **iso-**
For example iso-core, iso-gnome etc.

## Development Environment
Development environment is chroot for rlxos to setup rlxos-devel-environment on any linux/unix based operating system. and it should start with **devel-**
example devel-core, devel-python etc.

- execute rootfs.sh accordingly
```
    sudo ./scripts/rootfs <type>
```


# rlx-devel-env
To start the development enviroment execute.
```
    sudo ./scripts/start-devel-env.sh
```
and to copy files inside the rootfs
```
    sudo ./scripts/install-in-dev.sh <filepath> <?inside roots path>
```

